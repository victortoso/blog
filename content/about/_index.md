---
title: About Victor Toso
---

I am Brazilian by birth (from São Paulo) and Italian due my ancestry. I am
living in Brno - Czech Republic with my family. Here I work at Red Hat in the
[Spice](http://www.spice-space.org) project together with the Desktop team.

I try to manage my time by having fun with my family and working in projects
that excites me. I'm always interested in the great game of
[Go](https://en.wikipedia.org/wiki/Go_%28game%29) (even though I'm weak) and
also do-it-yourself and woodworking projects (even though I'm not doing much
myself).

This blog is about me sharing information related to projects I'm working on
although I plan to share travel and family information so my friends and family
in Brazil can lighten the distance a bit.

I'm mostly interested in projects related to multimedia and virtualization but
also [GNOME](https://gnome.org) which is where all the fun happens.

Note: This blog is also my playground to play with web related technologies when
I'm wiling or due any random necessity.

## Contact

* **Email**: me [at] victortoso [dot] com
* **IRC**: As toso in gimpnet and freenode
* **Keybase**: ![keybase-logo](/images/about-logo-keybase-16x16.png) [toso](https://keybase.io/toso/)
* **PGP key**: [97D9 123D E37A 484F](http://pgp.mit.edu/pks/lookup?op=get&search=0x97D9123DE37A484F)

## Social

As _victortoso_ in ![twitter-logo](/images/about-logo-twitter-16x16.png)
[twitter](https://twitter.com/victortoso/)
![gitlab-logo](/images/about-logo-gitlab-16x16.png)
[GitLab](https://gitlab.com/victortoso)
![github-logo](/images/about-logo-github-16x16.png)
[GitHub](https://github.com/victortoso)

